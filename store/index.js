import Vue from 'vue';
import Vuex from 'vuex';
import {setToken} from '@/utils/auth.js'

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		activeTabbar: 'home',
		userInfo: {}, // 用户信息
		appInfo: {}, // 系统信息
		detail: {}, // 详情
		adoptSetUp: {}, // 认养页面配置
		registerDetail: {}, // 注册详情 主要是为了页面之间数据传递
		curAddress: {}, // 当前选择地址
		curCoupon: {}, // 当前选中优惠券
		latLng: '', //
		goodsCategory: [], // 商品分类
		cartCount: -1, // 购物车数量
		cartDetail: [], // 当前购物的商品信息
		orderCount: { // 订单状态数量
			afterSale: 0,
			complete: 0,
			confirm: 0,
			deliver: 0,
			unPay: 0
		}
	},
	mutations: {
		// 更新tabbar
		updateActiveTabbar(state, activeTabbar) {
			state.activeTabbar = activeTabbar;
		},
		// 更新userInfo
		updateUserInfo(state, userInfo) {
			state.userInfo = userInfo;
		},
		// 更新appInfo
		updateAppInfo(state, appInfo) {
			state.appInfo = appInfo;
		},
		// 更新detail
		updateDetail(state, detail) {
			state.detail = detail;
		},
		// 更新adoptSetUp
		updateAdoptSetUp(state, adoptSetUp) {
			state.adoptSetUp = adoptSetUp;
		},
		// 更新registerDetail
		updateRegisterDetail(state, registerDetail) {
			state.registerDetail = registerDetail;
		},
		// 更新curAddress
		updateCurAddress(state, curAddress) {
			state.curAddress = curAddress;
		},
		// 更新curCoupon
		updateCurCoupon(state, curCoupon) {
			state.curCoupon = curCoupon;
		},
		// 更新latLng
		updateLatLng(state, latLng) {
			state.latLng = latLng;
		},
		// 退出
		updateLogout(state, latLng) {
			state.activeTabbar = 'home';
			state.userInfo = {};
			uni.reLaunch({
				url: '/pages/index/index'
			})
		},
		// 更新goodsCategory
		updateGoodsCategory(state, goodsCategory) {
			state.goodsCategory = goodsCategory;
		},
		// 更新cartCount
		updateCartCount(state, cartCount) {
			state.cartCount = cartCount;
		},
		// 更新cartDetail
		updateCartDetail(state, cartDetail) {
			state.cartDetail = cartDetail;
		},
		// 更新orderCount
		updateOrderCount(state, orderCount) {
			state.orderCount = orderCount;
		},
		
	},
	actions: {
		setActiveTabbar({ commit }, state) {
			const { activeTabbar } = state;
			commit('updateActiveTabbar', activeTabbar);
		},
		setUserInfo({ commit }, state) {
			const { userInfo, token } = state;
			setToken(token)
			commit('updateUserInfo', userInfo);
		},
		setAppInfo({ commit }, state) {
			const { appInfo } = state;
			commit('updateAppInfo', appInfo);
		},
		setDetail({ commit }, state) {
			const { detail } = state;
			commit('updateDetail', detail);
		},
		setAdoptSetUp({ commit }, state) {
			const { adoptSetUp } = state;
			commit('updateAdoptSetUp', adoptSetUp);
		},
		setRegisterDetail({ commit }, state) {
			const { registerDetail } = state;
			commit('updateRegisterDetail', registerDetail);
		},
		setCurAddress({ commit }, state) {
			const { curAddress } = state;
			commit('updateCurAddress', curAddress);
		},
		setCurCoupon({ commit }, state) {
			const { curCoupon } = state;
			commit('updateCurCoupon', curCoupon);
		},
		setLatLng({ commit }, state) {
			const { latLng } = state;
			commit('updateLatLng', latLng);
		},
		setLogout({ commit }, state) {
			commit('updateLogout');
		},
		setGoodsCategory({ commit }, state) {
			const { goodsCategory } = state;
			commit('updateGoodsCategory', goodsCategory);
		},
		setCartCount({ commit }, state) {
			const { cartCount } = state;
			commit('updateCartCount', cartCount);
		},
		setCartDetail({ commit }, state) {
			const { cartDetail } = state;
			commit('updateCartDetail', cartDetail);
		},
		setOrderCount({ commit }, state) {
			const { orderCount } = state;
			commit('updateOrderCount', orderCount);
		},
		
	}
})
export default store