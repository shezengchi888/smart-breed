export const appid = 'wx60b98900addb3d0f';

// 订单状态
export const orderStatus = {
	'已退款': '-1',
	'已取消': '0',
	'未支付': '1',
	'已支付': '2',
	'商家发货': '3',
	'已完成': '4',
}